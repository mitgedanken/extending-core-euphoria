-- test.ex

-- tests the stats library

--[[[Version: 4.0.5.2
--Author: C A Newbould
--Date: 2020.12.31
--Status: operational; incomplete
--Changes:]]]
--* allows for non-MS terminal use

include stats.e

procedure main(sequence title)
    puts(1, title & '\n')
    puts(1,repeat('-',length(title))& '\n')
    vector v = {1,2,3,4,5,6,7,8.8}
    puts(1, "The source vector (v) is ") ?v
    printf(1, "The arithmetic mean is %.1f\n", mean(v)) -- default
    printf(1, "The geometric mean is %.1f\n", mean(v,GEOM))
    printf(1, "The harmonic mean is %.1f\n", mean(v,HARM))
    puts(1, "Calling mean with invalid option: ")?mean(v,12)
    printf(1, "The standard deviation is %.1f\n", stddev(v))
    puts(1, "map(square,v) -> ") ?map(square,v)
    puts(1, "filter(ge,v,7) -> ") ?filter(ge,v,7)
    puts(1, "filter(even,v) -> ") ?filter(even, v)
    puts(1, "foldr1(add,v) -> ")?foldr1(add,v)
    puts(1, "foldr1(multiply,v) -> ")?foldr1(multiply,v)
    vector v2 = {4,5,6,7,8,9.2,17.5,0.3}
    puts(1, "The second vector (v2) is ") ?v2
    printf(1, "covariance(v,v2) is %.2f\n", covariance(v,v2))
    printf(1, "correlation(v,v2) is %.3f\n", correlation(v,v2))
    sequence cline = command_line()
    if match("euiw", cline[1]) or platform() != 2 then
        puts(1, "\n!!! Press ENTER to close !!!")
        getc(0)
    end if
end procedure

main("Testing functionally-defined statistics routines")

--[[[Version: 4.0.5.1
--Author: C A Newbould
--Date: 2020.09.13
--Status: operational; incomplete
--Changes:]]]
--* re-cast as main
--* added test of covariance & correlation
--* added title
--* allows for both euiw & eui

--[[[Version: 4.0.5.0
--Author: C A Newbould
--Date: 2020.08.16
--Status: operational; incomplete
--Changes:]]]
--* created
