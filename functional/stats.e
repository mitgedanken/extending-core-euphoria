--------------------------------------------------------------------------------
--	Library: stats.e
--------------------------------------------------------------------------------
-- Notes:
--
--
--------------------------------------------------------------------------------
--/*
--= Library: (functional)stats.e
-- Description: Programming library for a set of statistical operations
------
--[[[Version: 4.0.5.1
--Author: C A Newbould
--Date: 2020.09.13
--Status: operational; incomplete
--Changes:]]]
--* ##covariance## defined
--* ##correlation## defined
--* UNEQUAL code defined locally
--
------
--==A statistical extension library for Open Euphoria, using a functional
-- approach
--
-- This library serves two distinct purposes:
--* it provides an operational library for carrying out a range of basic
--  statistical functions
--* it illustrates an extension style to Euphoria which follows the principles
--  of functional programming
--
-- Utilise this type and the associated routine by adding the following
-- statement to your module:
--<eucode>include stats.e</eucode>
--
-- and, if necessary, a suitable //eu.cfg// file pointing to the library's
-- location.
--*/
--------------------------------------------------------------------------------
--/*
-- ----
--==Interface
--*/
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--
--=== Library files required
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--/*
-- 	Constants
--*/
--------------------------------------------------------------------------------
export enum ARITH, GEOM, HARM -- choice options for mean function, default ARITH
constant FALSE = (1=0)
constant TRUE = not FALSE
constant UNEQUAL = -9999
constant UNSET = -1
--------------------------------------------------------------------------------
--/*
-- 	Types
--*/
--------------------------------------------------------------------------------
type boolean(integer b) --> FALSE | TRUE
    return FALSE or TRUE
end type
--------------------------------------------------------------------------------
export type rid(integer i) --> routine_id
    return i >= UNSET
end type
--------------------------------------------------------------------------------
export type vector(sequence v) --> all-atom sequence
    for i = 1 to length(v) do
        if not atom(v[i]) then return FALSE
        else continue
        end if
    end for
    return TRUE
end type
--------------------------------------------------------------------------------
--/*
-- 	Variables
--*/
--------------------------------------------------------------------------------
export rid add = routine_id("add_")
export rid even = routine_id("even_")
export rid ge = routine_id("ge_")
export rid multiply = routine_id("multiply_")
export rid square = routine_id("sq_")
--------------------------------------------------------------------------------
--/*
-- Routines
--*/
--------------------------------------------------------------------------------
function add_(atom a1, atom a2) --> [atom]: a1 + a2
    return a1 + a2
end function
--------------------------------------------------------------------------------
export function covariance(vector v1, vector v2) --> [atom] cov(v1,v2)
    integer l = length(v1)
    if length(v2) = l then return sum(v1*v2)/l - mean(v1)*mean(v2)
    else puts(1, "!!! UNEQUAL LENGTHS !!!: ") return undefined(UNEQUAL)
    end if
end function
--------------------------------------------------------------------------------
export function correlation(vector v1, vector v2) --> [atom] corr(v1,v2)
    atom v = covariance(v1,v2)
    return iif(v != UNEQUAL, v/sqrt(variance(v1)*variance(v2)),v)
end function
--------------------------------------------------------------------------------
function even_(atom a) --> [boolean]: mod(a,2) = 0
    return not remainder(a,2)
end function
--------------------------------------------------------------------------------
export function filter(rid fn, vector list, object args = {}) --> [vector]: {list[i]} if fn(list[i])
    if atom(args) then args = {args}
    else args = args
    end if
    switch length(list) do
    case 0 then return list
    case else vector prev = filter(fn, list[1..$-1], args)
        return iif(call_func(fn, {list[$]} & args), append(prev, list[$]), prev)
    end switch
end function
--------------------------------------------------------------------------------
export function foldr(rid fn, object init, vector list) --> [object]: fn(list[n-1], list[n])
    switch length(list) do
    case 0 then return init
    case else return call_func(fn, {foldr(fn, init, list[1..$-1]), list[$]})
    end switch
end function
--------------------------------------------------------------------------------
export function foldr1(rid fn, vector list) --> [object]: fn(list[n-1], list[n])
    switch length(list) do
    case 0 then return undefined()
    case else return foldr(fn, list[1], list[2..$])
    end switch
end function
--------------------------------------------------------------------------------
function ge_(atom a1, atom a2) --> [atom]: a >= a2
    return a1 >= a2
end function
--------------------------------------------------------------------------------
export function iif(atom t, object true, object false) --> true | false
    if t then return true
    else return false
    end if
end function
--------------------------------------------------------------------------------
export function map(rid fn, vector list) --> [vector]: fn(list)
    switch length(list) do
    case 0 then return list
    case else return map(fn, list[1..$-1]) & call_func(fn, {list[$]})
    end switch
end function
--------------------------------------------------------------------------------
export function mean(vector v, integer typ = ARITH) --> [atom]: amean | gmean | hmean
    integer l = length(v)
    switch l do
    case 0 then return undefined()
    case else
        switch typ do
        case ARITH then return sum(v)/l
        case GEOM then return root(product(v),l)
        case HARM then return l/sum(recip(v))
        case else return undefined()
        end switch
    end switch
end function
--------------------------------------------------------------------------------
function multiply_(atom a1, atom a2) --> [atom]: a1 * a2
    return a1 * a2
end function
--------------------------------------------------------------------------------
export function product(vector v) --> [atom]: PI v
    return foldr(multiply,1,v)
end function
--------------------------------------------------------------------------------
function recip(object o) --> [object]: 1/o
    return 1/o
end function
--------------------------------------------------------------------------------
function root(atom a1, atom a2 = 2) --> [atom]: a2 root of a1
    return power(a1, recip(a2))
end function
--------------------------------------------------------------------------------
function sq_(atom a) --> [atom]: a*a
    return a*a
end function
--------------------------------------------------------------------------------
export function stddev(vector v) --> [atom]: sd v
    return root(variance(v))
end function
--------------------------------------------------------------------------------
export function sum(vector v) --> [atom]: SIGMA v
    return foldr(add,0,v)
end function
--------------------------------------------------------------------------------
function undefined(integer i = 0) --> [integer]: i
    puts(1, "*** UNDEFINED *** - ")
    return i
end function
--------------------------------------------------------------------------------
export function variance(vector v) --> [atom]: var v
    sequence d = v - mean(v)
    return sum(d*d) / length(v)
end function
--------------------------------------------------------------------------------
--
--=== Defined instance(s)
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--/*
-- ----
--*/
--------------------------------------------------------------------------------
--
-- Previous versions
--
--------------------------------------------------------------------------------
--[[[Version: 4.0.5.0
--Author: C A Newbould
--Date: 2020.08.16
--Status: operational; incomplete
--Changes:]]]
--* created
--* **vector** defined
--* ##foldr## defined
--* ##mean## defined
--* //ARITH// defined
--* ##filter## defined
--* ##sum## defined
--* ##product## defined
--* ##variance## defined
--* ##stddev## defined
--* ##filter## extended
--* ##foldr1## defined
--------------------------------------------------------------------------------
