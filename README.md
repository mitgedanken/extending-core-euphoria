# README #

This README outlines the different ways in which the functionality provided by the Core Euphoria interpreter can be extended.

Detail can be found in the Wiki.

### What is this repository for? ###

* To outline the different ways in which the core can be extended
* To give a fully-working example of each kind of extension
* To describe, in outline, extant examples of pre-coded projects using one, or more, of each type of extension

### How do I use this repository? ###

* Read the description of the different types in the Wiki
* For each working example study the code and read the documentation attached
* Read the details of the extension libraries that interest you
* Install a version of Euphoria, if you have not already done so, and use whichever libraries take your fancy

### Material in the repository###

* The folder *missing* contains a simple library module for dealing with true/false data
* The folder *functional* contains an example, in the form of a basic statistical library, of a set of functions defined in a declamatory manner

### Contribution guidelines ###

If you want your project to be added to the list then please provide the following:

* A current reference to the downloadable library source
* An outline of the purpose of the project, or a reference to where this can be found
* Detail of the extension strategies used
* A detail of the platform on which it works: OSs and Euphoria version(s)

### Who do I talk to? ###

* Repo owner

## Origin

This file was modified on 03 October 2020 by C A Newbould as v1.1.0.

This file was further modified on 10 November 2020 by C A Newbould as v1.1.1.
