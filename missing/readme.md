# The missing library

The most obvious omission in Core Euphoria is the lack of a true/false data type and any functionality using such a type.

The reasoning is probably that Euphoria has always taken a rather broad view of the "true" concept. In all versions all values bar zero are treated in Euphoria as representing "true" when used in an expression, with "false" represented solely by *0* (zero).

Nevertheless, many programming instances use a true/false test, so a **boolean** type is well worth defining.

## boolean.e

Hence the provision of a small library module, *boolean.e*, which contains definitions of *TRUE* and *FALSE*, **boolean** and *iif*, a function which wraps the *if..then..else* construct into a one-liner.

The library contents are fully documented in the file *boolean.e.html*.

## Origin

This file was created on 03 October 2020 by C A Newbould as v1.0.0.
